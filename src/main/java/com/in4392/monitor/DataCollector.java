package com.in4392.monitor;

import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.compute.PowerState;
import com.microsoft.azure.management.compute.VirtualMachine;
import com.microsoft.azure.management.compute.VirtualMachines;
import com.microsoft.azure.management.monitor.*;
import org.joda.time.DateTime;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Collector for VM usage data. It uses the Azure Java API. Data is stored using a DataManager.
 */
class DataCollector {

    private final Azure azure;
    private Thread collectorThread;
    private volatile boolean continueCollecting;
    private final DataManager dataManager;

    DataCollector (Azure azure, DataManager dataManager) {
        this.azure = azure;
        continueCollecting = false;
        this.dataManager = dataManager;
    }

    void startCollecting() {
        System.out.println("Starting data collection");
        continueCollecting = true;
        collectorThread = new Thread(this::collectData);
        collectorThread.start();
    }

    void stopCollecting() {
        System.out.println("Stopping data collection");
        continueCollecting = false;
        collectorThread.interrupt();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        try {
            dataManager.saveData();
            System.out.println("Data saved to disk");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Collects usage data every minute, which is the maximum frequency for the Azure Java API. Currently it only
     * records CPU usage.
     */
    private void collectData() {
        Set<VirtualMachine> runningVMsPrev = new HashSet<>();
        while (continueCollecting) {
            boolean failedCollecting = false;
            Set<VirtualMachine> runningVMs = runningVMs();

            Set<VirtualMachine> stoppedVMs = new HashSet<>(runningVMsPrev);
            stoppedVMs.removeAll(runningVMs);
            stoppedVMs.forEach(vm -> dataManager.removeRunningVM(vm.id()));

            Set<VirtualMachine> startedVMs = new HashSet<>(runningVMs);
            startedVMs.removeAll(runningVMsPrev);
            startedVMs.forEach(vm -> dataManager.addRunningVM(vm.id()));

            for (VirtualMachine vm : runningVMs) {
                for (MetricDefinition metricDefinition : azure.metricDefinitions().listByResource(vm.id())) {
                    if (metricDefinition.name().localizedValue().equalsIgnoreCase("Percentage CPU")) {
                        MetricCollection metricCollection = metricDefinition.defineQuery()
                                .startingFrom(DateTime.now().minusMinutes(2)).endsBefore(DateTime.now().minusMinutes(1))
                                .execute();
                        Metric metric = metricCollection.metrics().get(0);
                        TimeSeriesElement timeSeriesElement = metric.timeseries().get(0);
                        MetricValue data = timeSeriesElement.data().get(0);
                        failedCollecting = (data.average() == null || data.timeStamp() == null) || failedCollecting;
                        if (!failedCollecting) {
                            UsageData dataToAdd = new UsageData(data.timeStamp(), data.average());
                            boolean isNew = dataManager.add(vm.id(), dataToAdd);
                            System.out.println("Collected new data: " + isNew);
                            System.out.println("Collected data: " + dataToAdd + ", for VM: " + vm.id());
                        }
                        else {
                            System.out.println("No data available now for VM: " + vm.id());
                        }
                    }
                }
            }
            long sleepTime = failedCollecting ? 10_000L : 50_000L;
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                continueCollecting = false;
                Thread.currentThread().interrupt();
            }
            runningVMsPrev = runningVMs;
        }
    }

    /**
     * Gets all VMs that are currently running.
     * @return all VMs that have a PowerState of PowerState.RUNNING
     */
    private Set<VirtualMachine> runningVMs() {
        VirtualMachines allMachines = azure.virtualMachines();

        List<VirtualMachine> machineList = allMachines.list();
        return machineList.stream().filter(machine -> PowerState.fromInstanceView(machine.instanceView())
                .equals(PowerState.RUNNING)).collect(Collectors.toSet());
    }
}
