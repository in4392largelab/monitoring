package com.in4392.monitor;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.*;
import java.util.*;

/**
 * Manages the data that has been collected. It among other things manages the loading from and saving data to disk.
 * An inner class is contained for publishing data to other classes.
 */
class DataManager {
    private final Map<String, List<UsageData>> usageDataSet; // A map from Azure VM ID to a list of usage data instances
    private final String dataPath; // The file system path for storing and loading data to disk
    private final Set<String> runningVMs; // VMs that are currently running

    DataManager(String dataPath) {
        this.dataPath = dataPath;
        try {
            if (new File(dataPath).exists()) {
                usageDataSet = loadData();
                System.out.println("Data loaded from disk");
            }
            else {
                usageDataSet = new HashMap<>();
                System.out.println("No existing data file found, creating new file");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        runningVMs = new HashSet<>();
    }

    private Map<String, List<UsageData>> loadData() throws IOException {
        try (FileInputStream fileInputStream = new FileInputStream(dataPath)) {
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            //noinspection unchecked
            return (Map<String, List<UsageData>>) objectInputStream.readObject();
        } catch (FileNotFoundException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    void saveData() throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(dataPath)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(usageDataSet);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    boolean add(String vm, UsageData data) {
        return getOrAddVM(vm).add(data);
    }

    void addRunningVM(String vm) {
        runningVMs.add(vm);
    }

    void removeRunningVM(String vm) {
        runningVMs.remove(vm);
    }

    private List<UsageData> getOrAddVM(String vm) {
        if (!usageDataSet.containsKey(vm)) {
            usageDataSet.put(vm, new ArrayList<>());
        }
        return usageDataSet.get(vm);
    }

    /**
     * Publishes collected data to other classes
     */
    class MetricProvider {
        Set<String> runningVMs() {
            return new HashSet<>(runningVMs);
        }
        Set<String> allVMs() { return usageDataSet.keySet(); }

        /**
         * Returns the last known CPU usage value for the specified VM.
         */
        double lastCpuUsage(String vm) {
            if (!usageDataSet.containsKey(vm)) {
                throw new IllegalArgumentException("No usage data recorded for this VM");
            }
            List<UsageData> vmUsage = usageDataSet.get(vm);
            return vmUsage.get(vmUsage.size() - 1).cpuUsage;
        }

        /**
         * Returns the average CPU usage value over the specified past period, for the specified VM.
         * @param period the time period in the past from now that has to be considered
         * @throws NoRecordedDataException if no data has been recorded in the specified period.
         */
        double avgCpuUsage(String vm, Period period) throws NoRecordedDataException {
            if (!usageDataSet.containsKey(vm)) {
                throw new IllegalArgumentException("No usage data recorded for this VM");
            }
            List<UsageData> vmUsage = usageDataSet.get(vm);

            boolean withinPeriod = true;
            int index = vmUsage.size() - 1;
            double usageSum = 0;
            int dataCount = 0;
            while (withinPeriod && index >= 0) {
                UsageData currentData = vmUsage.get(index);
                if (currentData.time.isAfter(DateTime.now().minus(period))) {
                    usageSum += currentData.cpuUsage;
                    dataCount++;
                }
                else {
                    withinPeriod = false;
                }
                index--;
            }

            if (dataCount == 0) {
                throw new NoRecordedDataException("No data has been recorded for the requested time period");
            }

            return usageSum / dataCount;
        }
    }
}
