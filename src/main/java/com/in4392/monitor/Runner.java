package com.in4392.monitor;

import com.microsoft.azure.management.Azure;
import org.apache.commons.io.output.TeeOutputStream;

import java.io.*;

/**
 * Monitor application runner
 */
class Runner {

    /**
     *
     * @param args args[0]: Azure authentication file name; args[1]: data file path; args[2]: HTTP publish port number;
     *             args[3]: log file path
     */
    public static void main(String[] args) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(args[3], true);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        TeeOutputStream teeOutputStream = new TeeOutputStream(System.out, fileOutputStream);
        System.setOut(new PrintStream(teeOutputStream));

        System.out.println("Starting monitoring application at " + java.time.ZonedDateTime.now());
        String authenticationFile = args[0];
        File authFile = new File(authenticationFile);
        DataCollector dataCollector;
        DataManager dataManager = new DataManager(args[1]);
        try {
            dataCollector = new DataCollector(Azure.authenticate(authFile).withDefaultSubscription(),
                    dataManager);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        dataCollector.startCollecting();
        DataManager.MetricProvider metricProvider = dataManager.new MetricProvider();
        DataPublisher publisher = new DataPublisher(metricProvider);
        publisher.publish(Integer.parseInt(args[2]));
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            dataCollector.stopCollecting();
            System.out.println("Stopping monitoring application at " + java.time.ZonedDateTime.now());
        }));
    }

}
