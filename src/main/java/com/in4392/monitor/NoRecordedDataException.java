package com.in4392.monitor;

/**
 * Thrown when data is requested that has not been recorded.
 */
class NoRecordedDataException extends Exception{
    public NoRecordedDataException() {
    }

    public NoRecordedDataException(String message) {
        super(message);
    }

    public NoRecordedDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoRecordedDataException(Throwable cause) {
        super(cause);
    }

    public NoRecordedDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
