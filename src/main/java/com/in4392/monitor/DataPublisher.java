package com.in4392.monitor;

import org.joda.time.Period;

import static spark.Spark.get;
import static spark.Spark.port;

/**
 * Uses the Spark framework to publish the collected data via HTTP.
 */
class DataPublisher {
    private final DataManager.MetricProvider metricProvider;

    DataPublisher(DataManager.MetricProvider metricProvider) {
        this.metricProvider = metricProvider;
    }

    void publish(int port) {
        port(port);

        get("/ui/running-vms", (req, res) -> {
            String resultString = "Number of running VMs: " + metricProvider.runningVMs().size() + "<br /><br />";
            resultString += "Running VMs:<br />";
            resultString += metricProvider.runningVMs().stream().map(this::extractName).reduce((s1, s2) -> s1 + "<br />" +
                    s2).orElse("");
            return resultString;
        });

        get("/txt/running-vms/num", (req, res) -> metricProvider.runningVMs().size());
        get("/txt/running-vms/names", (req, res) ->
                metricProvider.runningVMs().stream().map(this::extractName).reduce((s1, s2) -> s1 + "," +
                        s2).orElse(""));

        get("/txt/cpu-usage/current/:vm-name", (req, res) -> {
           if (metricProvider.runningVMs().stream().noneMatch(vm -> extractName(vm).equals(req.params(":vm-name")))) {
               res.status(404);
               return "";
           }
           else {
               @SuppressWarnings("OptionalGetWithoutIsPresent") String fullID = metricProvider.runningVMs().stream().filter(vm -> extractName(vm)
                       .equals(req.params(":vm-name"))).findFirst().get();
               return Double.toString(metricProvider.lastCpuUsage(fullID));
           }
        });

        get("/txt/cpu-usage/past/:vm-name", (req, res) -> {
            if (metricProvider.allVMs().stream().noneMatch(vm -> extractName(vm).equals(req.params(":vm-name")))) {
                res.status(404);
                return "";
            }
            else {
                @SuppressWarnings("OptionalGetWithoutIsPresent") String fullID = metricProvider.allVMs().stream().filter(vm -> extractName(vm)
                        .equals(req.params(":vm-name"))).findFirst().get();
                int periodParam = Integer.parseInt(req.queryParams("period"));
                try {
                    return Double.toString(metricProvider.avgCpuUsage(fullID, Period.minutes(periodParam)));
                } catch (NoRecordedDataException e) {
                    res.status(404);
                    return "";
                }
            }
        });

        System.out.println("Data is published at port " + port);
    }

    private String extractName(String vmID) {
        @SuppressWarnings("SpellCheckingInspection") String[] splitted = vmID.split("/");
        return splitted[splitted.length - 1];
    }
}
