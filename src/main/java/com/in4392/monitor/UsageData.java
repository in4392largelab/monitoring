package com.in4392.monitor;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Objects;

/**
 * Data class containing data for one measurement.
 */
class UsageData implements Serializable {

    @SuppressWarnings("WeakerAccess")
    final DateTime time;
    @SuppressWarnings("WeakerAccess")
    final double cpuUsage; //Percentage

    UsageData(DateTime time, double cpuUsage) {
        this.time = time;
        this.cpuUsage = cpuUsage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsageData usageData = (UsageData) o;
        return Objects.equals(time, usageData.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time);
    }

    @Override
    public String toString() {
        return "UsageData{" +
                "time=" + time +
                ", cpuUsage=" + cpuUsage +
                '}';
    }
}
