# Azure monitoring application
This application collects usage data from all VMs accessible by the user for which an authorization file is provided.
Usage data is published via HTTP, as defined by the following simple API:

- `/txt/running-vms/num`: number of running VMs
- `/txt/running-vms/names`: comma-separated list of names of running VMs
- `/txt/cpu-usage/current/<vm-name>`: last known CPU usage value of the running VM `<vm-name>`
- `/txt/cpu-usage/past/<vm-name>?period=<period>`: average CPU usage of VM `<vm-name>` from `<period>` minutes before
now until now. If no usage is known during the specified period, a `404` error is returned.

Command line arguments:

1. Azure authentication file path
2. Data file path
3. HTTP port number for publishing data
